package sortinlower

import (
	"fmt"
	"sort"
	"strings"
	"testing"
)

//https://www.zhihu.com/question/592951384/answer/2991556249
func TestSortInLower(t *testing.T) {
	s := []string{"Go", "Bravo", "Gopher", "gopher1", "Gopher2", "Alpha", "Grin", "Delta"}
	//注释以下两行代码，测试不同排序结果
	//预计结果：[Alpha Bravo Delta Go Grin Gopher Gopher2 gopher1]
	// sort.Strings(s)
	//预计结果：[Alpha Bravo Delta Go Gopher gopher1 Gopher2 Grin]
	sort.Slice(s, func(i, j int) bool { return strings.ToLower(s[i]) < strings.ToLower(s[j]) })
	fmt.Println(s)
}
