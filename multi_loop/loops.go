package multiloop

import (
	"fmt"
	"testing"
)

func TestBreakOutter(t *testing.T) {
outter:
	for i := 0; i < 10; i++ {
		for j := 0; j < 10; j++ {
			if i == 5 {
				continue outter
			}
			fmt.Println("i:", i, "j:", j)
		}
	}
}
