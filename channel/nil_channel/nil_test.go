package nilchannel

import (
	"fmt"
	"testing"
	"time"
)

//https://www.zhihu.com/question/593571755/answer/2988285910

//无法从nil channel读取数据, 读取会阻塞，测试环境go version go1.18 windows/amd64
func TestReadFromNilChan(t *testing.T) {
	var c chan int
	select {
	case v := <-c:
		fmt.Println("read from nil channel:", v)
	case <-time.After(time.Second): //注释掉这行代码，测试阻塞
		fmt.Println("timeout")
	}
}

// 无法向nil channel写入数据, 写入会阻塞，测试环境go version go1.18 windows/amd64
func TestWritrToNilChan(t *testing.T) {
	var c chan int
	fmt.Println(c)
	select {
	case c <- 1:
		fmt.Println("write to nil channel")
	case <-time.After(5 * time.Second): //注释掉这行代码，测试阻塞
		fmt.Println("timeout")
	}
}
