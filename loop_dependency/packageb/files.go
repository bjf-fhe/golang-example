package packageb

type NameGetter interface {
	GetName() string
}

type Middle struct {
	NameGetter
	Name string
}

func (m *Middle) GetName() string {
	return m.Name + "." + m.NameGetter.GetName()
}
