package packagea

import (
	"fmt"
	"testing"

	packageb "gitee.com/bjf-fhe/golang-example/loop_dependency/packageb"
)

func TestLoopDependency(t *testing.T) {
	var top = Top{
		Name: "top",
		Middle: packageb.Middle{
			NameGetter: &Base{
				Name: "base",
			},
			Name: "middle",
		},
	}
	fmt.Println("top.GetName():", top.GetName())
}
