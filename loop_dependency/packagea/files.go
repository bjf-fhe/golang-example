package packagea

import (
	packageb "gitee.com/bjf-fhe/golang-example/loop_dependency/packageb"
)

type Base struct {
	Name string
}

func (b *Base) GetName() string {
	return b.Name
}

type Top struct {
	Name string
	packageb.Middle
}

func (t *Top) GetName() string {
	return t.Name + "." + t.Middle.GetName()
}
