package bson

import (
	"encoding/json"
	"fmt"
	"testing"
)

type TestData struct {
	Name string
	Age  int
	Desc string
}

// TestLowerKeyOfJson 测试json中的key为小写时，Golang中的struct中的字段为大写时，是否可以正常解析
// 相关讨论 https://www.zhihu.com/question/592094583/answer/2968533859
func TestLowerKeyOfJson(t *testing.T) {
	var str = `{"name":"test","age":10,"desc":"test desc with some long text"}`
	var data = TestData{}
	err := json.Unmarshal([]byte(str), &data)
	fmt.Println("data:", data, "err:", err)
}
