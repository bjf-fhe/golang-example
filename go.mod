module gitee.com/bjf-fhe/golang-example

go 1.18

require (
	go.mongodb.org/mongo-driver v1.11.4 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
)
