package decorator

type User struct {
	Name string
	Age  int
}

func (u *User) String() string {
	return "User:" + u.Name
}

func (u *User) GetAge() int {
	return u.Age
}

type Admin struct {
	User
}

func (a *Admin) String() string {
	return "Admin:" + a.Name
}
