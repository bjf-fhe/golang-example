package decorator

import (
	"fmt"
	"testing"
)

// https://www.zhihu.com/question/589194902/answer/2993657758
// GO 编程项目装饰器模式怎么实现？

func TestAdminString(t *testing.T) {
	a := Admin{User{"Tom", 18}}
	fmt.Println(a.String())
	fmt.Println(a.GetAge())
}
