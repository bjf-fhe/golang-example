package slice

import (
	"fmt"
	"testing"
)

//很多人认为切片的append操作会重新分配内存，但是实际上，当cap足够时，append操作不会重新分配内存
//相关讨论 https://www.zhihu.com/question/586990457/answer/2972220582

// TestSliceAppend 测试切片的append操作，当cap足够时，append操作不会重新分配内存
func TestSliceAppendWithEnoughCap(t *testing.T) {
	var a = make([]int, 3, 6)
	a[0] = 1
	a[1] = 2
	a[2] = 3
	fmt.Printf("%p\n", a)
	var b = []int{4, 5, 6}
	a = append(a, b...)
	fmt.Printf("%p\n", a)
}

// TestSliceAppend 测试切片的append操作，当cap不够时，append操作会重新分配内存
func TestSliceAppendWithSmallCap(t *testing.T) {
	var a = make([]int, 3, 3)
	a[0] = 1
	a[1] = 2
	a[2] = 3
	fmt.Printf("%p\n", a)
	var b = []int{4, 5, 6}
	a = append(a, b...)
	fmt.Printf("%p\n", a)
}
